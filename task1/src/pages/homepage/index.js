import React from 'react'
import List from '../../components/List'

const homepage = ({title}) => {
    return(
        <div>
            <h1>{title}</h1>
            <ol>
                <List dish="Piersi w śmietanie"/>
                <List dish="Spagetti"/>
                <List dish="Zupa z ciecierzycy"/>
                <List dish="Pieczona ryba z ryżem"/>
                <List dish="Pizza"/>
            </ol>
        </div>
    );
}

export default homepage;
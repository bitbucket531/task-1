import React from 'react'

const List = ({dish}) => {
    return(
        <li>{dish}</li>
    );
}

export default List;